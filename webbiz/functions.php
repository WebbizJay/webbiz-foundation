<?php
// Register Custom Navigation Walker
require_once('wp_bootstrap_navwalker.php');
//Add styles to site
function theme_styles() {
	wp_enqueue_style( 'slick_css', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.css' );
	wp_enqueue_style( 'fontawsome_css', '//maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css' );
	wp_enqueue_style( 'animate.css', '//cdnjs.cloudflare.com/ajax/libs/animate.css/3.4.0/animate.min.css' );
	wp_enqueue_style( 'bootstrap_css', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css' );
	wp_enqueue_style( 'main_css',get_stylesheet_directory_uri(). '/style.css' );
}
add_action( 'wp_enqueue_scripts', 'theme_styles' );
//Add scripts to site
function theme_scripts() {
	wp_enqueue_script('jquery');
	wp_enqueue_script( 'jquery-ui-core');
	wp_enqueue_script( 'modernizr_js', get_template_directory_uri() . '/js/modernizr-custom.js', array() );
    	wp_enqueue_script( 'slick_js', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.js', array('jquery'), '1.0.0', true );
    	wp_enqueue_script( 'bachstreach_js', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-backstretch/2.0.4/jquery.backstretch.min.js', array('jquery'), '1.0.0', true );
	wp_enqueue_script( 'waypoints_js', '//cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.0/jquery.waypoints.min.js', array('jquery'), '', true );
	wp_enqueue_script('sticky_js', 'https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.0/shortcuts/sticky.min.js', array('jquery', 'waypoints_js'), '', true );
	wp_enqueue_script( 'image_loaded_js', get_template_directory_uri().'/js/imagesloaded.pkgd.min.js', array('jquery'), null, true);
    	wp_enqueue_script( 'isotope_js', get_template_directory_uri().'/js/isotope.pkgd.min.js', array('jquery', 'image_loaded_js'), null, true);
	wp_enqueue_script( 'bootstrap_js', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js', '', '', true );
	wp_enqueue_script('g_maps_js', 'https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyDparAxZ__7dpnl13CiXya5GmGUDefjgO0', '', '', true );
	wp_enqueue_script( 'maps_js', get_template_directory_uri(). '/js/custom_maps.js', array('g_maps_js'), '', true );
	wp_enqueue_script( 'custom_js', get_stylesheet_directory_uri(). '/js/custom.js', '', '', true );
}
add_action( 'wp_enqueue_scripts', 'theme_scripts' );
function custom_css(){
	$source = get_stylesheet_directory_uri().'/css/custom.css';
	wp_enqueue_style('custom_css', $source, false,'1','all');
}
add_action('wp_head','custom_css',1);
// Hide Admin Bar 
show_admin_bar(false);
// Featured Images
add_theme_support('post-thumbnails');
// Menus Support
add_theme_support('menus' );
// Title Tags
add_theme_support( 'title-tag' );
// SVG ALLow
function custom_mtypes( $m ){
    $m['svg'] = 'image/svg+xml';
    $m['svgz'] = 'image/svg+xml';
    return $m;
}
add_filter( 'upload_mimes', 'custom_mtypes' );
//Change Read More
function new_excerpt_more( $more ) {
	return '.....';
}
add_filter( 'excerpt_more', 'new_excerpt_more' );
// ACF Custom Options Page
if ( function_exists( 'acf_add_options_page' ) ) {

	acf_add_options_page( array(
			'page_title'  => 'Theme General Settings',
			'menu_title' => 'Theme Settings',
			'menu_slug'  => 'theme-general-settings',
			'capability' => 'edit_posts',
			'redirect'  => false
		) );
}
// Custom Exerpts
	function wbstarter_excerpt($limit) {
	      $excerpt = explode(' ', get_the_excerpt(), $limit);
	      if (count($excerpt)>=$limit) {
	        array_pop($excerpt);
	        $excerpt = implode(" ",$excerpt).'...';
	      } else {
	        $excerpt = implode(" ",$excerpt);
	      } 
	      $excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
	      return $excerpt;
	}

	// Custom Content
	function wbstarter_content($limit) {
	      $excerpt = explode(' ', get_the_content(), $limit);
	      if (count($excerpt)>=$limit) {
	        array_pop($excerpt);
	        $excerpt = implode(" ",$excerpt).'...';
	      } else {
	        $excerpt = implode(" ",$excerpt);
	      } 
	      $excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
	      return $excerpt;
	}
// Widget Areas
// Add Footer Widgets to Theme
register_sidebar( array(
		'name' => 'Footer Column 1',
		'id' => 'footer-sidebar-1',
		'description' => 'Appears in the footer area',
		'before_widget' => '<div id="%1$s" class="%2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

register_sidebar( array(
		'name' => 'Footer Column 2',
		'id' => 'footer-sidebar-2',
		'description' => 'Appears in the footer area',
		'before_widget' => '<div id="%1$s" class="%2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

register_sidebar( array(
		'name' => 'Footer Column 3',
		'id' => 'footer-sidebar-3',
		'description' => 'Appears in the footer area',
		'before_widget' => '<div id="%1$s" class="%2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

register_sidebar( array(
		'name' => 'Footer Column 4',
		'id' => 'footer-sidebar-4',
		'description' => 'Appears in the footer area',
		'before_widget' => '<div id="%1$s" class="%2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

// get Field Key
/**
 * Get field key for field name.
 * Will return first matched acf field key for a give field name.
 * 
 * ACF somehow requires a field key, where a sane developer would prefer a human readable field name.
 * http://www.advancedcustomfields.com/resources/update_field/#field_key-vs%20field_name
 * 
 * This function will return the field_key of a certain field.
 * 
 * @param $field_name String ACF Field name
 * @param $post_id int The post id to check.
 * @return 
 */
function acf_get_field_key( $field_name, $post_id ) {
	global $wpdb;
	$acf_fields = $wpdb->get_results( $wpdb->prepare( "SELECT ID,post_parent,post_name FROM $wpdb->posts WHERE post_excerpt=%s AND post_type=%s" , $field_name , 'acf-field' ) );
	// get all fields with that name.
	switch ( count( $acf_fields ) ) {
		case 0: // no such field
			return false;
		case 1: // just one result. 
			return $acf_fields[0]->post_name;
	}
	// result is ambiguous
	// get IDs of all field groups for this post
	$field_groups_ids = array();
	$field_groups = acf_get_field_groups( array(
		'post_id' => $post_id,
	) );
	foreach ( $field_groups as $field_group )
		$field_groups_ids[] = $field_group['ID'];
	
	// Check if field is part of one of the field groups
	// Return the first one.
	foreach ( $acf_fields as $acf_field ) {
		if ( in_array($acf_field->post_parent,$field_groups_ids) )
			return $acf_fields[0]->post_name;
	}
	return false;
}
add_filter('acf/settings/google_api_key', function () {
    return 'AIzaSyDparAxZ__7dpnl13CiXya5GmGUDefjgO0';
});
?>