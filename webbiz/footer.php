			</main>
			<footer>
				<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 footer-block">
					<?php
						if(is_active_sidebar('footer-sidebar-1')){
						dynamic_sidebar('footer-sidebar-1');
						}
					?>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 footer-block">
					<?php
						if(is_active_sidebar('footer-sidebar-2')){
						dynamic_sidebar('footer-sidebar-2');
						}
					?>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 footer-block">
					<?php
						if(is_active_sidebar('footer-sidebar-3')){
						dynamic_sidebar('footer-sidebar-3');
						}
					?>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 footer-block">
					<?php
						if(is_active_sidebar('footer-sidebar-4')){
						dynamic_sidebar('footer-sidebar-4');
						}
					?>
				</div>
			</footer>
		<?php wp_footer(); ?>
	</body>
</html>
