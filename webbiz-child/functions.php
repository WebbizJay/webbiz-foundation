<?php
$theme_name = '(Client Name)';
// Main Menu
register_nav_menus( array(
    'primary' 		=> __( 'Primary Menu', '$theme_name' ),
    'mobile'			=> __('Mobile Menu', '$theme_name')	
) );
// Custom Post Include http://jjgrainger.co.uk/2013/07/15/easy-wordpress-custom-post-types/
include_once('CPT.php');
// SAMPLE CPT
// // create a Presentation custom post type
// $presentations = new CPT('presentation');
// // create a genre taxonomy
// $presentations->register_taxonomy('presentation_year');
// // use "pages" icon for post type
// $presentations->menu_icon("dashicons-media-document");
?>
