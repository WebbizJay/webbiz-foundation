<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
		<?php wp_head(); ?>
	</head>
	<body <?php body_class('animated fadeIn'); ?>>	
			<header>
				
			</header>
			<nav class="navbar navbar-default" role="navigation">
			  <div class="container-fluid">
			    <!-- Brand and toggle get grouped for better mobile display -->
			    <div class="navbar-header">
			      <button type="button" class="hamburger <?php echo get_field('mobile_menu_icon', 'options'); ?>" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="hamburger-box">
						<span class="hamburger-inner"></span>
					</span>
			      </button>
			      <a class="navbar-brand" href="<?php echo home_url(); ?>">
			      	<?php 
			      		$logo_header = get_field('logo_header', 'options');
			      		if ( !empty($logo_header) ):
			      	?>
						
						<img src="<?php echo $logo_header['url']; ?>" alt="<?php echo $logo_header['alt']; ?>">	
					
					<?php else: ?>
			      	
			                <?php bloginfo('name'); ?>

			            
			            <?php endif; ?>
			      </a>
			    </div>

			        <?php
			            wp_nav_menu( array(
						'menu'              => 'primary',
						'theme_location'    => 'primary',
						'depth'             => 2,
						'container'         => 'div',
						'container_class'   => 'collapse navbar-collapse',
						'container_id'      => 'bs-example-navbar-collapse-1',
						'menu_class'        => 'nav navbar-nav',
						'link_before' => '<span>',
                        		'link_after' => '</span>',
						'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
						'walker'            => new wp_bootstrap_navwalker())
			            );
			        ?>
			    </div>
			</nav>
			<main>
		
		
