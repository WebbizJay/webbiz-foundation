(function( $ ) {
	// Mobile Menu Icon Trigger
	function mobile_menu_trigger() {
		var $hamburger = $(".hamburger");
		$hamburger.on("click", function(e) {
		$hamburger.toggleClass("is-active");
			$('ADD A CLASS').slideToggle(300);
		});
	}
	
	/**
	* Call functions here when the page resizes
	*/
	$( window ).resize(function() {
		
	});
	
	/**
	* Call functions here when the page fully loads
	*/
	$(window).load(function() {
		
	});

	/**
	* Call other functions here
	*/
	mobile_menu_trigger();

	
}(jQuery));